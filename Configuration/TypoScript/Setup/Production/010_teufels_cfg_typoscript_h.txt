###################
#### HEADLINES ####
###################

####################
## DEFAULT HEADER ##
####################
lib.stdheader.10.default.setCurrent {
  field = header
  replacement {
  # get rid of hard breaks using regex for <br> and
  10 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.10.default.dataWrap = <h1{register:headerClass}><span>|</span></h1>

#######################
## DEFAULT SUBHEADER ##
#######################
lib.stdheader.20.default.current = 1
lib.stdheader.20.default.htmlSpecialChars = 0
lib.stdheader.20.default.setCurrent {
  field = subheader
  replacement {
  # get rid of hard breaks using regex for <br> and
  20 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.20.default.dataWrap = <h2{register:headerClass}><span>|</span></h2>
lib.stdheader.20.default.wrap = |


###############
## H1 HEADER ##
###############
lib.stdheader.10.1.setCurrent {
  field = header
  replacement {
  # get rid of hard breaks using regex for <br> and
  10 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.10.1.dataWrap = <h1{register:headerClass}><span>|</span></h1>

##################
## H1 SUBHEADER ##
##################
lib.stdheader.20.1.current = 1
lib.stdheader.20.1.htmlSpecialChars = 0
lib.stdheader.20.1.setCurrent {
  field = subheader
  replacement {
  # get rid of hard breaks using regex for <br> and
  20 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.20.1.dataWrap = <h2{register:headerClass}><span>|</span></h2>
lib.stdheader.20.1.wrap = |

###############
## H2 HEADER ##
###############
lib.stdheader.10.2.setCurrent {
  field = header
  replacement {
  # get rid of hard breaks using regex for <br> and
  10 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.10.2.dataWrap = <h2{register:headerClass}><span>|</span></h2>

##################
## H2 SUBHEADER ##
##################
lib.stdheader.20.2.current = 1
lib.stdheader.20.2.htmlSpecialChars = 0
lib.stdheader.20.2.setCurrent {
  field = subheader
  replacement {
  # get rid of hard breaks using regex for <br> and
  20 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.20.2.dataWrap = <h3{register:headerClass}><span>|</span></h3>
lib.stdheader.20.2.wrap = |

###############
## H3 HEADER ##
###############
lib.stdheader.10.3.setCurrent {
  field = header
  replacement {
  # get rid of hard breaks using regex for <br> and
  10 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.10.3.dataWrap = <h3{register:headerClass}><span>|</span></h3>

##################
## H3 SUBHEADER ##
##################
lib.stdheader.20.3.current = 1
lib.stdheader.20.3.htmlSpecialChars = 0
lib.stdheader.20.3.setCurrent {
  field = subheader
  replacement {
  # get rid of hard breaks using regex for <br> and
  20 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.20.3.dataWrap = <h4{register:headerClass}><span>|</span></h4>
lib.stdheader.20.3.wrap = |

###############
## H4 HEADER ##
###############
lib.stdheader.10.4.setCurrent {
  field = header
  replacement {
  # get rid of hard breaks using regex for <br> and
  10 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.10.4.dataWrap = <h4{register:headerClass}><span>|</span></h4>

##################
## H4 SUBHEADER ##
##################
lib.stdheader.20.4.current = 1
lib.stdheader.20.4.htmlSpecialChars = 0
lib.stdheader.20.4.setCurrent {
  field = subheader
  replacement {
  # get rid of hard breaks using regex for <br> and
  20 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.20.4.dataWrap = <h5{register:headerClass}><span>|</span></h5>
lib.stdheader.20.4.wrap = |

###############
## H5 HEADER ##
###############
lib.stdheader.10.5.setCurrent {
  field = header
  replacement {
  # get rid of hard breaks using regex for <br> and
  10 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.10.5.dataWrap = <h5{register:headerClass}><span>|</span></h5>

##################
## H5 SUBHEADER ##
##################
lib.stdheader.20.5.current = 1
lib.stdheader.20.5.htmlSpecialChars = 0
lib.stdheader.20.5.setCurrent {
  field = subheader
  replacement {
  # get rid of hard breaks using regex for <br> and
  20 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.20.5.dataWrap = <h6{register:headerClass}><span>|</span></h6>
lib.stdheader.20.5.wrap = |

###############
## H6 HEADER ##
###############
lib.stdheader.10.6.setCurrent {
  field = header
  replacement {
  # get rid of hard breaks using regex for <br> and
  10 {
   search = /(<br\ ?\/?>)/
   useRegExp = 1
   replace = </span><br /><span>
    }
  }
}
lib.stdheader.10.6.dataWrap = <h6{register:headerClass}><span>|</span></h6>